console.log('start');
var app = {
    collections: {},
    models: {},
    views: {}
};

(function getGalleries() {
    $.ajax({
        url: '/data/galleries.json',
        async: false,
        success: function (response) {
            response.galleries.forEach(function (gallery) {
                localStorage.setItem('gallery_' + gallery.id, JSON.stringify(gallery));
            });
        },
        error: function () {
            alert('Load error: galleries');
        }
    });
})();
function getData(dataType) {
    var data = localStorage.getItem(dataType);
    if (!data) {
        $.ajax({
            url: '/data/'+dataType+'.json',
            async: false,
            success: function (response) {
                localStorage.setItem(''+dataType, JSON.stringify(response));
            },
            error: function () {
                alert('Load error: '+dataType);
            }
        });
        data = localStorage.getItem(dataType);
    }
    return JSON.parse(data);
}

var app = {
    collections: {},
    models: {},
    views: {}
};

app.models.sort = Backbone.Model.extend({
    defaults: {
        'sort': 'none'
    }
});
app.views.sort = Backbone.View.extend({
    tagName: 'div',
    viewTpl: _.template($('#sort-template').html()),
    initialize: function () {
        this.render();
    },
    events : {
        'change' : function (event) {
            app.productShow.trigger('sort:'+event.target.value);
        }
    },    
    render: function () {
        this.$el.html(this.viewTpl(this.model.toJSON()));
        $('#sort').html(this.$el);
        return this;
    }
});

app.models.galleryImage = Backbone.Model.extend({
    initialize: function () {
        this.set('uri', '/images/'+this.get('galleryId')+'/'+this.get('name'));
    }
});
app.views.galleryImage = Backbone.View.extend({
    tagName: 'a',
    viewTpl: _.template($('#product-gallery-template').html()),
    render: function () {
        this.$el.html(this.viewTpl(this.model.toJSON()));
        return this;
    }
});
app.collections.galleryImage = Backbone.Collection.extend({
    model: app.models.galleryImage,
    render: function () {
        this.models.forEach(function( model ){
            console.log(model);
            var view = new app.views.galleryImage({ model: model });
            $('#gallery_'+model.get('galleryId')).append(view.render().el);
            $(".fancybox").fancybox({openEffect: 'none',closeEffect: 'none'});
        }, this);
        return this;
    }
});

app.models.product = Backbone.Model.extend({
    initialize: function () {
        var descriptionShort = this.get('description').substr(0,100)+'...';
        this.set({'descriptionShort' : descriptionShort});
        var titleShort = this.get('title').substr(0,20)+'...';
        this.set({'titleShort' : titleShort});
        this.set({'titleShort' : this.get('title').substr(0,10)+'...'});
        
        var galleryData = JSON.parse(localStorage.getItem('gallery_'+this.get('gallery_id')));
        var galleryImageModels = [];
        var galleryImageClass = 'mainImage';
        
        galleryData.images.forEach(function (galleryImageName){
            galleryImageModels.push(new app.models.galleryImage({
                'galleryId': galleryData.id,
                'name': galleryImageName,
                'galleryImageClass': galleryImageClass
            }));
            galleryImageClass = 'additionalImage';
        });
        this.set('gallery',new app.collections.galleryImage(galleryImageModels));
    },
    defaults: {
        "id": "",
        "title": "",
        "titleShort": "",
        "titleCart": "",
        "quantity": 0,
        "category_id": "",
        "gallery_id": "",
        "price": "",
        "description": "",
        "descriptionShort": "",
        "quantityInCart": 0
    }
});
app.views.product = Backbone.View.extend({
    tagName: 'div',
    viewTpl: _.template($('#product-template').html()),
    initialize: function () {
        this.on('soldout', this.soldout, this);
    },
    events : {
        'click .addToCart' : 'addToCart',
        'change .productCnt' : 'changeProductCnt'
    },
    render: function () {
        this.$el.html(this.viewTpl(this.model.toJSON()));
        this.model.get('gallery').render();

        if((Number(this.model.get('quantity'))-Number(this.model.get('quantityInCart'))) === 0) {
            this.trigger('soldout');
        }        
        return this;
    },
    addToCart: function() {
        var quantityChoosen = $('#qt_'+this.model.get('id')).val();
        this.model.set('quantityInCart', Number(this.model.get('quantityInCart'))+Number(quantityChoosen));
        this.render();
        app.cart.render();
        app.productsAll.save();
    },
    changeProductCnt: function(){
        var quantityChoosen = $('#qt_'+this.model.get('id')).val();
        if(Number(quantityChoosen) > (Number(this.model.get('quantity'))-Number(this.model.get('quantityInCart')))) {
            quantityChoosen = Number(this.model.get('quantity'))-Number(this.model.get('quantityInCart'));
        }
        if(Number(quantityChoosen) < 0) {
            quantityChoosen=0;
        }
        $('#qt_'+this.model.get('id')).val(quantityChoosen);
    },
    soldout: function(){
        $('#button_'+this.model.get('id')).attr('disabled', true);
    }
});
app.collections.product = Backbone.Collection.extend({
    model: app.models.product,
    initialize: function () {
        this.$el = $('#productList');
        this.on('sort:sortByPriceDesc', this.sortByPriceDesc, this);
        this.on('sort:sortByPriceAsc', this.sortByPriceAsc, this);
        this.on('categorySelected', this.filterByCategoryId, this);
        
        this.on('all', function(){
            console.log(arguments);
        });
    },
    render: function () {
        this.$el.html('');        
        this.models.forEach(function( model ){
            var productView = new app.views.product({ model: model });
            this.$el.append( productView.render().el);
            model.get('gallery').render();
        }, this);       
        return this;
    },
    sortByPriceDesc: function(){
        var models = this.sortBy(function(product){
            return -product.get('price');
        });
        this.models = models;
        this.render();
    },
    sortByPriceAsc: function(){
        var models = this.sortBy(function(product){
            return product.get('price');
        });
        this.models = models;
        this.render();
    },
    filterByCategoryId: function(categoryId){
        if(categoryId instanceof Array) categoryId = categoryId[0]

        var models = this.models.filter(function(product) {
            return (product.get('category_id') === categoryId) ? true : false;
        });
        app.sorting = new app.views.sort({model: new app.models.sort({'sort': 'none'})});        
        app.productShow = new app.collections.product(models);
        app.productShow.render();
    },
    filterForCart: function(){
        var models = this.models.filter(function(product) {
            return (product.get('quantityInCart') > 0) ? true : false;
        });
        if(!models) models = [];
        return models;
    },
    sumForCart: function(){
        var models = this.filterForCart();
        var sum = 0;
        models.forEach(function(model){
            sum += model.get('price')*model.get('quantityInCart');
        });
        return sum.toFixed(2);
    },
    save: function (){
        var products = [];
        this.models.forEach(function(model){
           products.push(model.attributes);
        });
        
        localStorage.setItem('products', JSON.stringify({'products' : products}));        
    }
});

app.models.category = Backbone.Model.extend();
app.views.category = Backbone.View.extend({
    tagName: 'div',
    events : {
        'click' : function (event) {
            app.productsAll.trigger('categorySelected', [this.model.get('id')]);
        }
    },      
    viewTpl: _.template($('#category-template').html()),
    initialize: function () {
        this.render();
    },
    render: function () {
        this.$el.html(this.viewTpl(this.model.toJSON()));
        return this;        
    }
});
app.collections.category = Backbone.Collection.extend({
    model: app.models.category,
    initialize: function () {
        this.$el = $('#categories');
    },
    render: function () {
        this.$el.html('');
        this.models.forEach(function( category ){
            var categoryView = new app.views.category({model: category });
            this.$el.append( categoryView.render().el);
        }, this);       
        return this;
    }
});

app.views.cart = Backbone.View.extend({
    tagName: 'div',
    events : {
    },      
    viewTpl: _.template($('#cart-template').html()),
    initialize: function () {
        this.$el = $('#cart');
        this.render();
    },
    render: function () {
        var obj = {totalSum: app.productsAll.sumForCart()};
        this.$el.html(this.viewTpl(obj));
        $('#cartProduct').html('');
        var models = app.productsAll.filterForCart();
        models.forEach(function(model){
            var cartProductView = new app.views.cartProduct({model: model})
            $('#cartProduct').append(cartProductView.$el);
        },this);
        return this;        
    }        
});
app.views.cartProduct = Backbone.View.extend({
    tagName: 'div',
    events : {
        'change .productCnt' : 'changeProductCnt'        
    },      
    viewTpl: _.template($('#cart-product-template').html()),
    initialize: function () {
        this.render();
    },    
    render: function () {
        this.$el.html(this.viewTpl(this.model.toJSON()));
        return this;
    },
    changeProductCnt: function(){
        console.log(this.model);
        var quantityChoosen = $('#qtct_'+this.model.get('id')).val();
        if(Number(quantityChoosen) > (Number(this.model.get('quantity')))) {
            quantityChoosen = Number(this.model.get('quantity'));
        }
        if(Number(quantityChoosen) < 0) {
            quantityChoosen=0;
        }
        this.model.set('quantityInCart', quantityChoosen);
        console.log(this.model);        
        app.productShow.render();
        app.cart.render();
        app.productsAll.save();
    }    
});

var categoryModels = [];
(getData('categories'))['categories'].forEach(function(datum){
    categoryModels.push(new app.models.category(datum));
});
var productModels = [];
(getData('products'))['products'].forEach(function(productDatum){
    productModels.push(new app.models.product(productDatum));
});

app.category = new app.collections.category(categoryModels);
app.category.render();
app.productsAll = new app.collections.product(productModels);
app.productShow = app.productsAll;
app.productShow.render();
app.productsCart = new app.collections.product(productModels);
app.sorting = new app.views.sort({model: new app.models.sort({'sort': 'none'})});
app.cart = new app.views.cart();